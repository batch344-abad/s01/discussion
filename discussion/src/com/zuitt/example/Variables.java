package com.zuit.example;
// A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
// packages are divided into two categories:
// 1. Built - in packages (Packages from the JavaAPI) like in OpenJDK
// 2. User-defined - packages : Creates by a developer. or self-made.

//Package creation in Java follows the 'reverse domain name notation' for the naming convention.


public class Variables {
    public static void main (String[] args){
        //Naming Convention
        // the terminology used for variable names is identifier.
        // all identifiers should begin with a letter or character, it can also start with currency($) or an underscore. As long as it is not a number, it will be good.
        // After the first character, identifiers can have any combination of characters.
        // Most importantly, identifiers are case sensitive.

        // Variable declaration(datatype variable_name):
        int age;
        char middleName;


        //Variable declaration together with initialization:
        int x;
        int y = 0;

        //initialization after declaration
        x = 1;
        //output to the system:
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive Data Types:
        //predefined within the Java Programming language which is used for single-valued variables with limited capabilities.

        // int - whole number values;
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long - L is added to end of the long number to be recognized;
        long worldPopulation = 48989123123123L;
        System.out.println(worldPopulation);

        //Floating value
        //float add f at the end of the value. it can hold up to 7 decimal places.
        float piFloat = 3.14159265359f;
        System.out.println("The value of piFloat is " + piFloat);

        //double - floating values
        double piDouble = 3.14159265359;
        System.out.println("The value of piDouble is " + piDouble);

        //char - single character. it uses single qoute
        char letter = 'a';
        System.out.println(letter);

        //boolean - true or false value
        boolean isLove = true;
        boolean taken = false;
        System.out.println(isLove);
        System.out.println(taken);

        //constants
        //Java uses the "final" keyword so that variable's value cannot be changed; it is also appropriate to capitalize the name of the variable/identifier.

        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);


        // Non - primitive Data
        // also known as reference data types refers to instances or objects.
        // does not directly store the value of a variable but rather remember the reference to that variable.

        // String - stores a sequence or array of characters. also an object that can use methods. like dot method.
        String username = "Jay";
        System.out.println(username);

        //sample string method
        int stringLength = username.length();
        System.out.println(stringLength);





    }
}
